#!/bin/bash

clone_linux() {
    if [ ! -d "linux-stm32mp" ]; then
        git clone git@github.com:OneKiwiPublic/linux-stm32mp.git -b onekiwi-v5.10-stm32mp-r2.2
    fi
}

build_linux() {
    cd linux-stm32mp
    export KBUILD_OUTPUT=./build
    make multi_v7_defconfig
    make ${DEVICE_NAME}.dtb -j8
}

source ./scripts/build-sdk.sh

clone_linux
build_linux